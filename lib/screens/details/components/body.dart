import 'package:e_commerce/components/default_button.dart';
import 'package:e_commerce/components/rounded_icon_btn.dart';
import 'package:e_commerce/models/Cart.dart';
import 'package:e_commerce/models/Product.dart';
import 'package:e_commerce/screens/cart/cart_screen.dart';
import 'package:collection/collection.dart';
import 'package:e_commerce/models/final_price_calculator.dart';
import 'package:flutter/material.dart';
import '../../../constants.dart';
import '../../../size_config.dart';
import 'color_dots.dart';
import 'product_description.dart';
import 'top_rounded_container.dart';
import 'product_images.dart';
import 'package:e_commerce/screens/details/components/color_dots.dart';

class Body extends StatefulWidget {
  late final Product product;
  Body({Key? key, required this.product}) : super(key: key);
  int quantity = 0;
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  @override
  Widget build(BuildContext context) {
    int selectedColor = 3;
    int index = widget.product.id;
    return ListView(
      children: [
        ProductImages(product: widget.product),
        TopRoundedContainer(
          color: Colors.white,
          child: Column(
            children: [
              ProductDescription(
                product: widget.product,
                pressOnSeeMore: () {},
              ),
              TopRoundedContainer(
                color: Color(0xFFF6F7F9),
                child: Column(
                  children: [
                    Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: getProportionateScreenWidth(20)),
                      child: Row(
                        children: [
                          ...List.generate(
                            widget.product.colors.length,
                            (index) => ColorDot(
                              color: widget.product.colors[index],
                              isSelected: index == selectedColor,
                            ),
                          ),
                          Spacer(),
                          RoundedIconBtn(
                            icon: Icons.remove,
                            press: () {
                              setState(() {
                                widget.quantity = widget.quantity - 1;
                              });
                            },
                          ),
                          SizedBox(width: getProportionateScreenWidth(5)),
                          Text(widget.quantity.toString()),
                          SizedBox(width: getProportionateScreenWidth(5)),
                          RoundedIconBtn(
                            icon: Icons.add,
                            showShadow: true,
                            press: () {
                              setState(() {
                                widget.quantity = widget.quantity + 1;
                              });
                            },
                          ),
                        ],
                      ),
                    ),
                    // ColorDots(product: product),
                    TopRoundedContainer(
                      color: Colors.white,
                      child: Padding(
                        padding: EdgeInsets.only(
                          left: SizeConfig.screenWidth * 0.15,
                          right: SizeConfig.screenWidth * 0.15,
                          bottom: getProportionateScreenWidth(40),
                          top: getProportionateScreenWidth(15),
                        ),
                        child: SizedBox(
                          width: double.infinity,
                          height: getProportionateScreenHeight(56),
                          child: TextButton(
                            style: TextButton.styleFrom(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20)),
                              primary: Colors.white,
                              backgroundColor: kPrimaryColor,
                            ),
                            onPressed: () {
                              demoCarts.add(Cart(
                                product: demoProducts[index],
                                numOfItem: widget.quantity,
                              ));
                              finalPrice.add((demoProducts[index].price *
                                  widget.quantity));
                              PriceCalculator cal = new PriceCalculator();
                              Navigator.pushNamed(
                                context,
                                ExtractArgumentsScreen.routeName,
                                arguments: CartScreen(
                                  price: cal.getFinalPrice(),
                                ),
                              );
                            },
                            child: Text(
                              "Add to Cart",
                              style: TextStyle(
                                fontSize: getProportionateScreenWidth(18),
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
