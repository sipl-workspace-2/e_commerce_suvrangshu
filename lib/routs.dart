import 'package:e_commerce/screens/cart/cart_screen.dart';
import 'package:e_commerce/screens/cataloge_screen/home_screen.dart';
import 'package:e_commerce/screens/details/details_screen.dart';
import 'package:e_commerce/screens/sign_up/sign_up_screen.dart';
import 'package:flutter/cupertino.dart';

import 'screens/splashScreen/splash_screen.dart';
import 'screens/sign_in/sign_in_screen.dart';

final Map<String, WidgetBuilder> routes = {
  SplashScreen.routeName: (context) => SplashScreen(),
  SignInScreen.routeName: (context) => SignInScreen(),
  SignUpScreen.routeName: (context) => SignUpScreen(),
  HomeScreen.routeName: (context) => HomeScreen(),
  DetailsScreen.routeName: (context) => DetailsScreen(),
  ExtractArgumentsScreen.routeName: (context) => ExtractArgumentsScreen(),
};
